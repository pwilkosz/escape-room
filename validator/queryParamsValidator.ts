import { ValidatorInterfaceStrategy } from './ValidatorInterfaceStrategy';
import * as validator from 'jsonschema';
import { queryParamsJsonSchema } from './schema/queryParamsJsonSchema';

export class QueryParamsValidator implements ValidatorInterfaceStrategy {

  private schema;
  private errorMessages: string[];

  public constructor() {
    this.schema = queryParamsJsonSchema;
  }

  public isMatch(data): boolean {
    return true;
  }

  public getErrorMessages():string[] {
    return this.errorMessages;
  }
  public isValid(data): boolean {
    let val = new validator.Validator();
    let result = val.validate(data, this.schema);
    if (result.errors.length !== 0 ) {
      this.errorMessages = result.errors.map((error => error['stack']));
      return false;
    }
    return true;
  }
}
