export const reservationJsonSchema = {
  "required": ["reservationDate"],
  "properties": {
    "reservationDate":{
      "type": "string",
      "format": "date-time",
      "minLength": 5,
      "maxLength": 50
    }
  }
}
