export const addRoomJsonSchema = {
  "required": ["name", "city", "street", "country", "phoneNumber", "rating", "capacity"],
  "properties": {
    "name":{
      "type": "string",
      "minLength": 5,
      "maxLength": 50,
      "pattern": "^[a-zA-Z0-9\s]+$"
    },
    "city":{
      "type": "string",
      "minLength": 5,
      "maxLength": 50,
      "pattern": "^[a-zA-Z0-9\s]+$"
    },
    "street":{
      "type": "string",
      "minLength": 5,
      "maxLength": 50,
      "pattern": "^[a-zA-Z0-9\s]+$"
    },
    "country":{
      "type": "string",
      "minLength": 5,
      "maxLength": 20,
      "pattern": "^[a-zA-Z0-9\s]+$"
    },
    "phoneNumber":{
      "type": "string",
      "minLength": 5,
      "maxLength": 50,
      "pattern": "[\d\s]*"
    },
    "ratig":{
      "type": "number",
      "minimum": 0,
      "maximum": 5
    },
    "capacity":{
      "type": "number",
      "minimum": 0,
      "maximum": 10
    },
  }
}
