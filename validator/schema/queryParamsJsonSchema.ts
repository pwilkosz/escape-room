export const queryParamsJsonSchema = {
  "properties": {
    "city":{
      "type": "string",
      "minLength": 2,
      "maxLength": 50
    },
    "lte":{
      "type": "string",
      "pattern": "^\\d{1,2}$"
    },
    "gte":{
      "type": "string",
      "pattern": "^\\d{1,2}$"  
    },
  }
}
