export class ValidationResult {
  public constructor(
    public valid: boolean,
    public messages: string[]
  ){}
}
