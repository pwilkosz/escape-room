import { ValidatorInterfaceStrategy } from './ValidatorInterfaceStrategy';
import { addRoomJsonSchema } from './schema/addRoomJsonSchema';
import * as validator from 'jsonschema';

export class AddRoomValidator implements ValidatorInterfaceStrategy {

  private schema;
  private errorMessages: string[];

  public constructor() {
    this.schema = addRoomJsonSchema
  }

  public isMatch(data): boolean {
    return !data.reservationDate;
  }

  public getErrorMessages():string[] {
    return this.errorMessages;
  }
  public isValid(data): boolean {
    let val = new validator.Validator();
    let result = val.validate(data, this.schema);
    if (result.errors.length !== 0 ) {
      this.errorMessages = result.errors.map((error => error['stack']));
      return false;
    }
    return true;
  }
}
