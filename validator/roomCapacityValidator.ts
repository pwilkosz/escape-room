import { ValidatorInterfaceStrategy } from './ValidatorInterfaceStrategy';
import * as validator from 'jsonschema';

export class RoomCapacityValidator implements ValidatorInterfaceStrategy {

  private errorMessages: string[];

  public constructor() {
      this.errorMessages = [];
  }

  public isMatch(data): boolean {
    return !!data.lte && !!data.gte;
  }

  public getErrorMessages():string[] {
    return this.errorMessages;
  }
  public isValid(data): boolean {
    this.errorMessages = [];
    if (+data.lte < +data.gte) {
      this.errorMessages.push('Maximum room capacity should not be lower than minimum one.');
      return false;
    }
    return true;
  }
}
