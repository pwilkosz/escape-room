export interface ValidatorInterfaceStrategy {
  isMatch<T>(data:T): boolean;
  isValid<T>(data:T);
  getErrorMessages(): string[];
}
