import { ValidationServiceInterface } from './ValidationServiceInterface';
import { ValidatorInterfaceStrategy } from '../../validator/ValidatorInterfaceStrategy';
import { ValidationResult } from '../../validator/ValidationResult';

export class ValidationService implements ValidationServiceInterface{
    public constructor(
      public validators: ValidatorInterfaceStrategy[]
    ){}
    public validate(data) {
      let validationResult = new ValidationResult(
        true,
        []
      );
      this.validators.forEach(validator => {
          if(validator.isMatch(data)) {
            if(!validator.isValid(data)){
                validationResult.valid = false;
                validator.getErrorMessages().forEach(error => {
                      validationResult.messages.push(error);
                });

            }
          }
      });

      return validationResult;
    }
}
