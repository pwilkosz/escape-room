import { ValidationResult } from '../../validator/ValidationResult';

export interface ValidationServiceInterface {
  validate(data): ValidationResult;
}
