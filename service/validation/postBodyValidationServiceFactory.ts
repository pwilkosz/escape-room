import { ValidationService } from './ValidationService';
import { AddRoomValidator } from '../../validator/addRoomValidator';
import { ReservationValidator } from '../../validator/reservationValidator';

export class PostBodyValidationServiceFactory {
  public create(): ValidationService {
    return new ValidationService(
      [new AddRoomValidator(), new ReservationValidator()]
    );
  }
}
