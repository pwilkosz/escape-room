import { ValidationService } from './ValidationService';
import { RoomCapacityValidator } from '../../validator/roomCapacityValidator';
import { QueryParamsValidator } from '../../validator/queryParamsValidator';

export class QueryParamsValidationServiceFactory {
  public create(): ValidationService {
    return new ValidationService(
      [new QueryParamsValidator(), new RoomCapacityValidator()]
    );
  }
}
