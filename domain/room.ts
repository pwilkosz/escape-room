import { dateObject } from './dateObject';

export class Room {

  public reservationDates: dateObject[];

  public constructor(
    public name: string,
    public city: string,
    public street: string,
    public country: string,
    public phoneNumber: string,
    public rating: number,
    public capacity: number
  ) {
    this.reservationDates = [];
  }

}
