import { mongoRepository } from '../../repository/mongoRepository';
import { Room } from '../../domain/room';

describe('mongoRepository', () => {
  it('should return created object from database', () => {
    const room = new Room(
      "gawelek",
      "gawelek",
      "gawelek",
      "gawelek",
      "123123",
      7,
      6
    );
    const insertFn = jest.fn( () => {
        return Promise.resolve({
          result: {
            ok: 1,
            n: 1
          },
          ops: [
            {
              name: 'Słodki lol',
              city: 'Strzelin',
              street: 'Brzegowa 100',
              country: 'Poland',
              phoneNumber: '123456789',
              capacity: 5,
              reservationDates: [],
              _id: "5abfb7e76b4b6a452760aaaa" } ],
              insertedCount: 1,
              insertedIds: { '0': "5abfb7e76b4b6a452760aaaa" } })
            } );

    let db = {
        collection: () => {
          return {
              insert: insertFn
          }
        }
    }

    const repository: mongoRepository = new mongoRepository(db);

    return repository.insert(room)
    .then(res => {
      expect(res).toHaveProperty('name');
      expect(res).toHaveProperty('city');
      expect(res).toHaveProperty('street');
      expect(res).toHaveProperty('country');
      expect(res).toHaveProperty('phoneNumber');
      expect(res).toHaveProperty('capacity');
      expect(res).toHaveProperty('reservationDates');
      expect(res).toHaveProperty('_id');
      expect(insertFn).toBeCalled();
    })

  });

  it('should return entity from database', () => {
    const room = new Room(
      "gawelek",
      "gawelek",
      "gawelek",
      "gawelek",
      "123123",
      7,
      6
    );
    const getByIdFn = jest.fn( () => {
        return Promise.resolve(
          [
            {
              name: 'Słodki lol',
              city: 'Strzelin',
              street: 'Brzegowa 100',
              country: 'Poland',
              phoneNumber: '123456789',
              capacity: 5,
              reservationDates: [],
              _id: "5abfb7e76b4b6a452760aaaa" } ])
            } );

    let db = {
        collection: () => {
          return {
              find: () => {
                return {
                    toArray: getByIdFn
                }
              }
          }
        }
    }

    const repository: mongoRepository = new mongoRepository(db);

    return repository.getById('5abfb5536b4b6a452760aaa8')
    .then(res => {
      expect(res).toHaveProperty('name');
      expect(res).toHaveProperty('city');
      expect(res).toHaveProperty('street');
      expect(res).toHaveProperty('country');
      expect(res).toHaveProperty('phoneNumber');
      expect(res).toHaveProperty('capacity');
      expect(res).toHaveProperty('reservationDates');
      expect(res).toHaveProperty('_id');
      expect(getByIdFn).toBeCalled();
    })

  });

})
