
export interface FactoryInterface<T> {
  create(): Promise<T>
}
