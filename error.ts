
export class ApiException extends Error {
  public constructor(
    public code: number,
    public message: string
  ) {
    super(message);
  }
}
