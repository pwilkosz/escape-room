import * as express from 'express';
import * as bodyParser from 'body-parser';
import { mongoRepository } from './repository/mongoRepository';
import { Room } from './domain/room';
import * as mongo from 'mongodb';
import { FactoryInterface } from './Factory/FactoryInterface';
import { MongoRepositoryFactory } from './repository/mongoRepositoryFactory';
import * as dotenv from 'dotenv';
import { PostBodyValidationServiceFactory } from './service/validation/postBodyValidationServiceFactory';
import { QueryParamsValidationServiceFactory } from './service/validation/queryParamsValidationServiceFactory';

dotenv.config();
const app = express();
app.use(bodyParser.json());
const postBodyValidator = (new PostBodyValidationServiceFactory).create();
const queryStringValidator = (new QueryParamsValidationServiceFactory).create();
const mongoFactory: FactoryInterface<mongoRepository> = new MongoRepositoryFactory();

let repository: mongoRepository;

mongoFactory.create()
.then(client => {
  repository = client;
  app.get('/rooms', (req, res) => {
    let validationResult = queryStringValidator.validate(req.query);
    if (!validationResult.valid) {
      res.status(422).send({message: validationResult.messages});
    } else {
      repository.find(req.query.city, +req.query.gte, +req.query.lte)
        .then(result => {
          res.send(result);
        }).catch(error => {
          res.status(404).send(error);
    });
    }

  });

  app.get('/rooms/:id', (req, res, next) => {
    let objectId = mongo.ObjectId;
    if (!objectId.isValid(req.params.id)) {
      res.status(422).send({message: "Invalid id format, should be hex string 24 characters long"});
      next();
    }
    repository.getById(req.params.id)
    .then(result => {
      res.send(result);
    }).catch(error => {

      res.status(error.code).send({message: error.message});
    })
  });

  app.post('/rooms', (req, res, next) => {
    let validationResult = postBodyValidator.validate(req.body);
    if (!validationResult.valid) {
      res.status(422).send({message: validationResult.messages});
      next();
    }
    const room = new Room(
      req.body.name,
      req.body.city,
      req.body.street,
      req.body.country,
      req.body.phoneNumber,
      req.body.rating,
      req.body.capacity
    );
    repository.insert(room).then( result => {
      res.send(result);
    }).catch(err => {
      res.status(500).send({message: err});
      next();
    });


  });

  app.post('/rooms/:id/reservation', function(req, res, next) {
    let validationResult = postBodyValidator.validate(req.body);
    if (!validationResult.valid) {
      res.status(422).send({message: validationResult.messages});
      next();
    }

    let objectId = mongo.ObjectId;

    if (!objectId.isValid(req.params.id)) {
      res.status(422).send({message: "Invalid id format, should be hex string 24 characters long"});
      next();
    }
    let reservationDate = new Date(req.body.reservationDate);
    reservationDate.setHours(reservationDate.getHours() + Math.round(reservationDate.getMinutes()/60));
    reservationDate.setMinutes(0);
    reservationDate.setSeconds(0);
    reservationDate.setMilliseconds(0);

    repository.book(req.params.id, reservationDate).then( result => {
      res.send(result);
    }).catch(err => {
      res.status(err.code).send({message: err.message});
    });

  });

  app.listen(8080, () => console.log('Example app listening on port 8080!'))

}).catch(err => {
  console.log(`ERROR: ${err}`);
});
