import { escapeRoomsRepositoryInterface } from './escapeRoomsRepositoryInterface';
import { Room } from '../domain/room';
import { Db } from 'mongodb';
import { ApiException } from '../error';
import * as Mongo  from 'mongodb';
import * as _ from 'lodash';

export class mongoRepository implements escapeRoomsRepositoryInterface {

  public constructor(private db) {}

  public insert(room: Room){
    const collection = this.db.collection('rooms'); //put name of colelction there
    return collection.insert({
      name: room.name,
      city: room.city,
      street: room.street,
      country: room.country,
      phoneNumber: room.phoneNumber,
      capacity: room.capacity,
      reservationDates: []
    }).then(result => {
      return result.ops[0];
    }).catch (err => {
      throw Error(err);
    });
  }

  public getById(id: string){
    const collection = this.db.collection('rooms');
    let objId = new Mongo.ObjectId(id);
    return collection.find({'_id': objId}).toArray()
      .then(result => {
        if (result.length == 0) {
          throw new ApiException(
            404,
            'Room has not been found'
          );
        }
        return result[0];
      }).catch(err => {
        throw err;
      })

  }

  public find(city?: string, minCapacity?: number, maxCapacity?: number){
    let query = {};
    if (city) {
      query['city'] = city;
    }
    if (minCapacity) {
      _.set(query, 'capacity.$gte', minCapacity);
    }
    if (maxCapacity) {
      _.set(query, 'capacity.$lte', maxCapacity);
    }
    const collection = this.db.collection('rooms');
    return collection.find(query).toArray()
      .then(result => {
        return result;
      }).catch(err => {
        throw new Error(err);
      })
  }

  public book(id: string, date: Date) {
    const collection = this.db.collection('rooms');
    let objId = new Mongo.ObjectId(id);
    return collection.updateOne({_id: objId}, { $addToSet: { reservationDates: date } })
      .then(result => {
        if (result.matchedCount === 0) {
          throw new ApiException(
            404,
            'Room has not been found'
          );
        }
        if (result.modifiedCount === 0) {
          throw new ApiException(
            400,
            'reservation exists'
          );
        }

        return {
          message: 'Reservation has been completed.'
        };
      }).catch(err => {
        throw err;
      })
  }
}
