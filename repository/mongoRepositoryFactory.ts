import { FactoryInterface } from '../Factory/FactoryInterface';
import { mongoRepository } from './mongoRepository';
import { MongoClient } from 'mongodb';

export class MongoRepositoryFactory implements FactoryInterface<mongoRepository> {
  public create(): Promise<mongoRepository> {
    return MongoClient.connect(`mongodb://${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}`)
    .then(client => {
      console.log(process.env.MONGO_DB_NAME);
      return new mongoRepository(client.db(process.env.MONGO_DB_NAME))
    })
    .catch(err => { throw Error(err) } );
  }
}
