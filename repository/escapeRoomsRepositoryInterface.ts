import { Room } from '../domain/room';

export interface escapeRoomsRepositoryInterface {
  insert(room: Room);

  getById(id: string);

  find(city?: string, minCapacity?: number, maxCapacity?: number);

  book(id: string, date: Date); 
}
